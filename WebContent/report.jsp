<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib
	prefix="s"
	uri="/struts-tags"%>
<html>
<head>
<meta
	http-equiv="Content-Type"
	content="text/html; charset=UTF-8">
<link
	rel="stylesheet"
	href="${pageContext.request.contextPath}/css/global.css?v=4">
<script
	type="text/javascript"
	src="${pageContext.request.contextPath}/js/moment.js"></script>
<title>Reports</title>
</head>
<body>
	<s:form>
		<s:textfield
			id="startDate"
			label="Start date"
			name="startDateString"
			placeholder="%{getText('dateField.startDatePlaceholder')}" />
		<s:textfield
			id="endDate"
			label="End date"
			name="endDateString"
			placeholder="%{getText('dateField.endDatePlaceholder')}" />
		<s:select
			label="Performer"
			headerKey="-1"
			headerValue="All performers"
			list="performers"
			name="performer" />
		<s:select
			id="timePeriod"
			label="Time period"
			headerKey="-1"
			headerValue=""
			list="#{
			'LQ' : 'Last Qtr',
			'LM' : 'Last Month',
			'LY' : 'Last Calendar Year',
			'YTD' : 'Current Year to Date',
			'QTD' : 'Current Qtr to Date',
			'MTD' : 'Current Month to Date'
			}"
			name="timePeriod"
			onchange="fillDates()" />
		<s:submit value="Submit"></s:submit>
	</s:form>
	<s:if test="%{reports != null}">
		<s:if test="%{reports.isEmpty()}">
			<span class="errorMessage">
				<s:text name="msg.noReportsFound" />
			</span>
		</s:if>
		<s:else>
			<table class="table-data">
				<tr>
					<th>ID</th>
					<th>Start date</th>
					<th>End date</th>
					<th>Performer</th>
					<th>Activity</th>
				</tr>
				<s:iterator value="reports">
					<tr>
						<td>
							<s:property value="id" />
						</td>
						<td>
							<s:date
								name="startDate"
								format="MMM d, yyyy" />
						</td>
						<td>
							<s:date
								name="endDate"
								format="MMM d, yyyy" />
						</td>
						<td>
							<s:property value="performer" />
						</td>
						<td>
							<s:property value="activity" />
						</td>
					</tr>
				</s:iterator>
			</table>
		</s:else>
	</s:if>
	<script type="text/javascript">
		var startDateField;
		var endDateField;

		var currentDate;
		var dateFormat = 'MMM D, YYYY';

		var lastQuarterAdjustment;
		var currentQuarterAdjustment;

		var lastQuarterEndDate;
		var lastQuarterStartDate;
		var lastMonthEndDate;
		var lastMonthStartDate;
		var lastYearEndDate;
		var lastYearStartDate;

		var currentMonthStartDate;
		var currentQuaterStartDate;
		var currentYearStartDate;

		window.onload = function() {
			startDateField = document.getElementById("startDate");
			endDateField = document.getElementById("endDate");

			currentDate = moment();
			lastQuarterAdjustment = (currentDate.month() % 3) + 1;
			currentQuarterAdjustment = lastQuarterAdjustment - 1;

			lastQuarterEndDate = currentDate.clone().subtract(lastQuarterAdjustment, 'months').endOf('month');
			lastQuarterStartDate = lastQuarterEndDate.clone().subtract(2, 'months').startOf('month');

			lastMonthEndDate = currentDate.clone().subtract(1, 'months').endOf('month');
			lastMonthStartDate = lastMonthEndDate.clone().startOf('month');

			lastYearEndDate = currentDate.clone().subtract(1, 'years').endOf('year');
			lastYearStartDate = lastYearEndDate.clone().startOf('year');

			currentMonthStartDate = currentDate.clone().startOf('month');
			currentQuaterStartDate = currentDate.clone().subtract(currentQuarterAdjustment, 'months').startOf('month');
			currentYearStartDate = currentDate.clone().startOf('year');
		}

		function fillDates() {

			var timePeriod = document.getElementById("timePeriod").value;

			if (timePeriod == 'LQ') {

				startDateField.value = lastQuarterStartDate.format(dateFormat);
				endDateField.value = lastQuarterEndDate.format(dateFormat);

			} else if (timePeriod == 'LM') {

				startDateField.value = lastMonthStartDate.format(dateFormat);
				endDateField.value = lastMonthEndDate.format(dateFormat);

			} else if (timePeriod == 'LY') {

				startDateField.value = lastYearStartDate.format(dateFormat);
				endDateField.value = lastYearEndDate.format(dateFormat);

			} else {

				endDateField.value = currentDate.format(dateFormat);

				if (timePeriod == 'YTD') {
					startDateField.value = currentYearStartDate.format(dateFormat);
				} else if (timePeriod == 'QTD') {
					startDateField.value = currentQuaterStartDate.format(dateFormat);
				} else if (timePeriod == 'MTD') {
					startDateField.value = currentMonthStartDate.format(dateFormat);
				} else {
					startDateField.value = '';
					endDateField.value = '';
				}
			}
		}
	</script>
</body>
</html>