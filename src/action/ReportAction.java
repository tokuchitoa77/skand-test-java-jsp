package action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;

import model.ReportSearchData;
import model.entity.Report;
import model.exception.DateValidationException;
import service.ReportService;

@Action("report")
@Results({ @Result(name = "input", location = "/report.jsp"),
		@Result(name = "success", location = "/report.jsp") })
public class ReportAction extends ActionSupport implements ServletRequestAware {

	private static final long serialVersionUID = 1L;

	private static final ReportService reportService = new ReportService();

	// Used to prevalidate input date strings
	private static final Pattern datePattern = Pattern.compile("(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \\d{1,2}, \\d{4}");

	// Wrap SimpleDateFormat to make it thread-safe
	// Used to parse input date strings
	private static final ThreadLocal<SimpleDateFormat> dateFormat = new ThreadLocal<SimpleDateFormat>() {

		@Override
		protected SimpleDateFormat initialValue() {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH);
			// To prevent dates like "May 33, 1017"
			simpleDateFormat.setLenient(false);
			return simpleDateFormat;
		};
	};

	private HttpServletRequest httpServletRequest;

	private List<String> performers = reportService.getPerformers();
	private List<Report> reports;

	private String performer;
	private String startDateString;
	private String endDateString;
	private String timePeriod;

	@Override
	public String execute() {
		if ("POST".equals(httpServletRequest.getMethod())) {
			try {
				ReportSearchData reportSearchData = new ReportSearchData();
				reportSearchData.setStartDate(parseDate(startDateString));
				reportSearchData.setEndDate(parseDate(endDateString));
				reportSearchData.setPerformer("-1".equals(performer) ? null : performer);
				reports = reportService.get(reportSearchData);
			} catch (DateValidationException e) {
				e.printStackTrace();
				return "input";
			}
		}
		return "success";
	}

	@Override
	public void validate() {
		try {
			parseDate(startDateString);
		} catch (DateValidationException e) {
			addFieldError("startDateString", e.getMessage());
		}
		try {
			parseDate(endDateString);
		} catch (DateValidationException e) {
			addFieldError("endDateString", e.getMessage());
		}
	}

	private Date parseDate(String dateString) throws DateValidationException {
		if (dateString == null || "".equals(dateString)) {
			return null;
		}

		if (!datePattern.matcher(dateString).matches()) {
			throw new DateValidationException(getText("dateFiled.formatErrorMessage"));
		}

		Date date = null;
		try {
			date = dateFormat.get().parse(dateString);
		} catch (ParseException e) {
			throw new DateValidationException(getText("dateFiled.invalidDateErrorMessage"));
		}

		return date;
	}

	@Override
	public void setServletRequest(HttpServletRequest httpServletRequest) {
		this.httpServletRequest = httpServletRequest;
	}

	public List<String> getPerformers() {
		return performers;
	}

	public List<Report> getReports() {
		return reports;
	}

	public void setPerformer(String performer) {
		this.performer = performer;
	}

	public void setStartDateString(String startDateString) {
		this.startDateString = startDateString;
	}

	public void setEndDateString(String endDateString) {
		this.endDateString = endDateString;
	}

	public String getPerformer() {
		return performer;
	}

	public String getStartDateString() {
		return startDateString;
	}

	public String getEndDateString() {
		return endDateString;
	}

	public String getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(String timePeriod) {
		this.timePeriod = timePeriod;
	}

}