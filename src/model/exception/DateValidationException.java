package model.exception;

public class DateValidationException extends Exception {

	public DateValidationException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;

}
