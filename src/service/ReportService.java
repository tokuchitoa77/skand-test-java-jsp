package service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.hibernate.Session;
import org.hibernate.query.Query;

import model.ReportSearchData;
import model.entity.Report;

public class ReportService {

	public List<Report> get(ReportSearchData reportSearchData) {
		String performer = reportSearchData.getPerformer();
		Date startDate = reportSearchData.getStartDate();
		Date endDate = reportSearchData.getEndDate();

		Map<String, Object> parameters = new HashMap<>();
		String hql = "from Report r where 1 = 1";

		if (performer != null && !performer.equals("-1")) {
			hql += " and r.performer = :performer";
			parameters.put("performer", performer);
		}
		if (startDate != null) {
			hql += " and r.startDate >= :startDate";
			parameters.put("startDate", startDate);
		}
		if (endDate != null) {
			hql += " and r.endDate <= :endDate";
			parameters.put("endDate", endDate);
		}
		hql += " order by r.startDate";

		List<Report> result = null;
		Session session = HibernateService.getCurrentSession();
		try {
			session.getTransaction().begin();

			Query<Report> query = session.createQuery(hql, Report.class);
			parameters.forEach((k, v) -> query.setParameter(k, v));
			result = query.getResultList();

			session.getTransaction().commit();
		} catch (Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}

		return result;
	}

	/***
	 * 
	 * @return distinct values of Performer column
	 */
	public List<String> getPerformers() {
		List<String> result = null;
		Session session = HibernateService.getCurrentSession();
		try {
			session.getTransaction().begin();

			Query<String> query = session.createQuery("select distinct r.performer from Report r order by r.performer", String.class);
			result = query.getResultList();

			session.getTransaction().commit();
		} catch (Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}

		return result;
	}

	/***
	 * This method is used to fill database Report table with some random test-data
	 */
	public void fillDatabaseWithDummyReports() {
		Random rand = new Random();
		LocalDate dateNow = LocalDate.now();

		Session session = HibernateService.getCurrentSession();
		try {
			session.getTransaction().begin();

			for (int i = 0; i < 500; i++) {
				LocalDate randomEndDate = dateNow.minusDays(rand.nextInt(365));
				LocalDate randomStartDate = randomEndDate.minusDays(rand.nextInt(100));

				Report report = new Report();
				report.setPerformer("performer " + rand.nextInt(10));
				report.setStartDate(Date.from(randomStartDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
				report.setEndDate(Date.from(randomEndDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
				report.setActivity("activity " + rand.nextInt(10));

				session.persist(report);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			if (session.getTransaction().isActive()) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
	}

}
