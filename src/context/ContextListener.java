package context;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import service.HibernateService;
import service.ReportService;

public class ContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		new ReportService().fillDatabaseWithDummyReports();
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		HibernateService.closeSessionFactory();
	}

}
